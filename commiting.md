## Version control
### A good commit
All code should be under version control to keep track of all changes and who made them. This allows to compare versions of files, and thus try out different approaches without polluting a stable system with experiments. Ultimately, you can also undo changes. While many systems are available, the most popular by far is `git`, which will be the example used throughout this document.

Let's assume for the moment that you are working on a single branch (managing branches is left to the next section). In practice, you need to commit each time you finish a well-defined task (or close an issue). For example, this can be: creating a function, fixing a bug, or adding a test. When commiting, you will be asked to enter a message. Your message should follow the following structure:


- a header: `type`: short description or subject
- a body: more details about the commit
- a footer: did the commit close an issue ?

Each of these item must be separated by a blank line. Only the header is mandatory.

```sh
type : subject
[blank line]
body
[blank line]
footer
```



#### Header
In the header, the `type` should define what kind of modification you have done. It should start with one of the following keywords in lower case (if your commit involves several of them, then you should pick up the uppermost in this list although ideally you should reduce your commit to only one type):

- **feat**: add new functionality (or analysis)
- **fix**: fix a bug
- **refactor**: change code structure
- **test**: test part of code
- **perf**: performance improvement
- **design**: in case there is a aesthetical part
- **doc**: update documentation or comments
- **content**: images, text, sound for a user
- **typo**: correct spelling in doc
- **noise**: whitespace, indenting

Following the `type`, you need to define the subject very concisely of your commit. The subject should:

- Start with an action verb 
- Use the present imperative
- Be written in lower case
- Not finish with a period
- be less than 50 characters

#### Body 
The body carries more details about your commit such as results, improvement to be done, planned next steps etc. It is often longer than the subject (less than 100 characters), but follow all the other rules applied to the header.

#### Footer
The footer is a statement which will close an opened issue (if any). For example:

```sh
Close #28
```

indicates that this commit addresses the issue number 28 which can be now considered as closed. This will automatically close the issue in github when the branch you are working on will be merged on the master branch.


#### Example
Let's say you have conducted an exploratory analysis in which you have created a plot showing the correlation between age and height. Then your commit should look like:

```sh
feat: plot linear fit between age and height

coefficient=2.34, intercept=3.1. Do not appear to be linear, need to conduct non linear fit

close #23
```


Note that the only time your commit message deviates from these rules are the first commit which should be exactly:

```
initial commit
```


### Branching
#### When and how to fork
A branch is a version of a repository which can be run independantly from the master branch. It is useful if you want to develop a feature without being disturbed by development of peers working on the same repo. It also helps to isolate the development of different features. When you are finished with your task, you can merge it to another branch.

A good repo should *at least* contains 2 branches:

- master: version for public release
- develop (or explore): version for internal development


![alt text](content/develop_branch.png "An example of a simple branching structure")

Ideally, no matter if you fix a bug, or commit a feature, you should always:

- fork the develop branch
- commit your changes
- merge your new branch to the develop branch when an issue is closed

Once merged, you can delete the branch you were working on (except develop and master). Only very rarely, you should merge develop -> master. In practice, you should make this merge only when the product is ready to be submitted to the client. Don't forget to release a version of your repo at a milestone of your project (eg a MVP). Naming a release is also subject to a [convention](http://semver.org/). This will help you identify where to come back with a working version if an important bug is introduced later on.


![alt text](content/feature_branch.png "An example of a simple branching structure")



#### Naming convention
Many conventions exist, but one way is to name a branch under the form: TYPE/NN/short_description.

- TYPE: one of the word introduced in the header of the commit message.
- NN is the eventual number of the issue adressed.
- short_description: 3 words max to describe the content of your branch separated by underscores.

Example: `feat/32/binarize_target` or `refactor/call_api`





## Integrate your product backlog in github with zenhub
In a collaborative agile development, you might need to create a product backlog (which is basically the list of features your product should contain with their priorities and estimated difficulties) arranged on a taskboard to keep track of your progress, and estimate the velocity of your team during a cycle. A chrome extension can incorporate the taskboard to the git environment, Zenhub. It is however a paid functionnality for non-public repository.


![alt text](content/taskboard.jpg "A theoretical taskboard at the beginning of a sprint.")


![alt text](content/zenhub.png "Taskboard in the middle of a sprint in zenhub.")


## Version control of data
The basic Version control follows the *code*, and not the data. Thus, `.gitignore` exclude all temporary emacs/python files, and data (eg: `.csv`). In order to version data files (even small ones), you should use git lfs.

1. Follow the [instructions to install git-lfs](https://git-lfs.github.com/)  
2. `bash$ git lfs track "*.csv"` (kind of opposite of `.gitignore`)  
3. Use regular git commands (eg: `bash$ git commit file.csv`) to version your data.  



